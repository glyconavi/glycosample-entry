import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-location/iron-location.js';

class GlycosampleEntry extends PolymerElement {
  static get template() {
    return html`
    <style>
/* shadow DOM styles go here */:
host {
    display: inline - block;
}

table.TwoWayBack {
    border - collapse: collapse;
    text - align: left;
    line - height: 1.5;
    border - top: 10 px solid gray;
    border - bottom: 1 px solid gray;
    align: center;
    vertical - align: middle;
}
table.TwoWayBack thead th {
    width: 150 px;
    padding: 10 px;
    font - weight: bold;
    vertical - align: middle;
    color: #fff;
    background: #04162e;
}
table.TwoWayBack td {
	width: 650px;
	padding: 10px;
	vertical-align: middle;
	text-align: left;
}

table.TwoWayBack tr:hover {
	background-color: palegreen;
}

table.TwoWayBack tr {
	border-bottom-color: green;
	vertical-align: middle;
}

td {
	word-break:break-all;
	text-align: left;
	vertical-align: middle;
}

table.TwoWayBack tr:nth-child(even) {
	background-color: gainsboro;
}

table.TwoWayBack img {
	width:300px;
}

body h1, h2, h3, h4, iframe, footer {
	text-align: center;
}

iframe {
	border: 1px darkgreen solid;
}

.green-bord {
	background-color: # 82 ae46;
    color: #00552e;
}

.green-bord-black {
	background-color: # cee4ae;
    color: black;
}

div {
    align: center;
    text - align: center;
    width: 100 % ;
    vertical - align: middle;
}
    </style>

<!-- GlycoNavi SampleList Component -->
<iron-ajax auto="" url="https://sparqlist.glyconavi.org/api/glycosample_entry?[[query]]" handle-as="json" last-response="{{resultdata}}"></iron-ajax>
<iron-location path="{{path}}" hash="{{hash}}" query="{{query}}" dwell-time="{{dwellTime}}">
</iron-location>
<div><h2>Entry ID: GlycoNAVI:[[retrieveId(query)]]</h2></div>
<div>
  <table class="TwoWayBack">
    <thead>
      <tr>
        <th scope="cols">Entry</th>
        <th scope="cols">Data</th>
      </tr>
    </thead>
    <tbody>
      <template is="dom-repeat" items="[[resultdata]]">
        <tr>
          <th scope="row">[[item.entry]]</th>
          <td>[[item.data]]</td>
        </tr>
      </template>
    </tbody>
  </table>
</div>
`;
  }

  static get is() {
    return 'glycosample-entry';
  }
  static get properties() {
    return {
      id: {
        type: String
      },
      resultdata: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      }
    }
  }
  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }

  retrieveId(query) {
    console.log(query);
    var params = query.split("=");

    return params[1];
  }
}
customElements.define(GlycosampleEntry.is, GlycosampleEntry);
