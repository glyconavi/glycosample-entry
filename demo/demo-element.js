import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../glycosample-entry.js';
class DemoElement extends PolymerElement {
  static get template() {
    return html`
    <style>
    </style>

    <h3>GlycoNavi Sample Entry Demo</h3>

    <glycosample-entry id="GS_1"></glycosample-entry>
`;
  }

  static get is() { return "demo-element" }
}

customElements.define(DemoElement.is, DemoElement);
